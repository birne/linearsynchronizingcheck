﻿#pragma once
#include <vector>
#include "TypesDeclarations.h"
#include "Condensation.h"

class CondensationBuilder {
	CondensationBuilder() {}

	CondensationBuilder(const CondensationBuilder&);
	CondensationBuilder& operator =(CondensationBuilder&);

	static Graph BuildInvertedGraph(const Graph& graph) {
		Graph invertedGraph;

		invertedGraph.resize(graph.size());
		for (int i = 0; i < graph.size(); ++i) {
			for (int j = 0; j < graph[i].size(); ++j) {
				invertedGraph[graph[i][j]].push_back(i);
			}
		}

		return invertedGraph;
	}

	static void OrderDfs(const Graph& graph, int v, std::vector<bool>& used, std::vector<int>& order) {
		used[v] = true;

		for (int i = 0; i < graph[v].size(); ++i) {
			int to = graph[v][i];

			if (!used[to])
				OrderDfs(graph, to, used, order);
		}

		order.push_back(v);
	}

	static void ClusterDfs(const Graph& graph, int v, std::vector<bool>& used, std::vector<int>& cluster, int clusterNumber) {
		used[v] = true;
		cluster[v] = clusterNumber;

		for (int i = 0; i < graph[v].size(); ++i) {
			int to = graph[v][i];

			if (used[to])
				continue;

			ClusterDfs(graph, to, used, cluster, clusterNumber);
		}
	}

public:
	static CondensationBuilder& GetInstance() {
		static CondensationBuilder instance;
		return instance;
	}

	Condensation BuildCondensation(const Graph& graph, const std::vector<int>& letters) const {
		Graph condensation;
		std::vector<int> order;
		std::vector<bool> used;
		std::vector<int> cluster;

		int n = graph.size();

		order.resize(0);

		used.assign(n, false);
		for (int i = 0; i < n; ++i) {
			if (!used[i])
				OrderDfs(graph, i, used, order);
		}
		std::reverse(order.begin(), order.end());

		auto invertedGraph = BuildInvertedGraph(graph);
		used.assign(n, false);
		cluster.assign(n, -1);
		int clusterNumber = 0;
		for (int i = 0; i < n; ++i) {
			int v = order[i];
			if (!used[v])
				ClusterDfs(invertedGraph, v, used, cluster, clusterNumber++);
		}

		condensation.resize(clusterNumber);
		for (int v = 0; v < n; ++v) {
			for (int i = 0; i < letters.size(); ++i) {
				int letter = letters[i];
				int to = graph[v][letter];

				if (cluster[v] == cluster[to])
					continue;

				condensation[cluster[v]].push_back(cluster[to]);
			}
		}

		return Condensation(condensation, cluster);
	}
};
