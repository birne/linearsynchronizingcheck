﻿#pragma once
#include <vector>
#include "TypesDeclarations.h"

class Condensation {
public:
	Graph condensation;
	std::vector<int> initialVerticiesColoring;
private:
	int minComponentIndex;
	int minComponentSize;

public:
	Condensation(const Graph& condensation, const std::vector<int>& initialVerticiesColoring)
		: condensation(condensation), initialVerticiesColoring(initialVerticiesColoring) {
		minComponentIndex = -1;
		minComponentSize = -1;
	}

	bool IsSingleMin() const {
		int minCount = 0;
		for (int i = 0; i < condensation.size(); ++i) {
			if (condensation[i].size() == 0)
				++minCount;
		}
		return minCount <= 1;
	}

	int GetMinComponentIndex() {
		if (minComponentIndex != -1)
			return minComponentIndex;

		for (int i = 0; i < condensation.size(); ++i) {
			if (condensation[i].size() == 0)
				minComponentIndex = i;
		}

		return minComponentIndex;
	}

	int GetMinComponentSize() {
		if (minComponentSize != -1)
			return minComponentSize;

		minComponentSize = 0;
		for (int i = 0; i < initialVerticiesColoring.size(); ++i) {
			if (initialVerticiesColoring[i] == GetMinComponentIndex())
				++minComponentSize;
		}

		return minComponentSize;
	}
};
