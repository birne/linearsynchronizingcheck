﻿#pragma once
#include "TypesDeclarations.h"
#include <algorithm>

class AutomataStatesPair {
public:
	int GetP() const {
		return p;
	}

	int GetQ() const {
		return q;
	}

	int GetDependLetter() const {
		return dependLetter;
	}

	AutomataStatesPair() {
		p = q = dependLetter = -1;
	}

	AutomataStatesPair(int p, int q, int dependLetter) :
		p(std::min(p, q)), q(std::max(p, q)), dependLetter(dependLetter) { }

	bool operator <(const AutomataStatesPair& other) const {
		return p < other.p ||
			p == other.p && q < other.q;
	}

	AutomataStatesPair GetNext(const Graph& graph, int letter) const {
		return AutomataStatesPair(graph[p][letter], graph[q][letter], letter);
	}

private:
	int p, q;
	int dependLetter;
};
