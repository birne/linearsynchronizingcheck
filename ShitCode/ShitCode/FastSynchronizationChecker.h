﻿#pragma once
#include "TypesDeclarations.h"
#include "SlowSynchronizationChecker.h"
#include "ClusterGraph.h"
#include "StablePairsSets.h"
#include "CondensationBuilder.h"

class FastSynchronizationChecker {
	FastSynchronizationChecker() {}

	FastSynchronizationChecker(const FastSynchronizationChecker&);
	FastSynchronizationChecker& operator =(FastSynchronizationChecker&);
public:
	enum FastSynchrinizationTestResult {
		DontKnow = 0,
		Synchronizable = 1,
		NonSynchronizable = 2
	};

	static bool CheckTheorem2Case1(const Graph& graph, const ClusterStructure& clusterStructureA, const ClusterStructure& clusterStructureB, const std::vector<int>& letters) {
		for (int i = 0; i < clusterStructureA.GetClusterCount(); ++i) {
			auto cluster = clusterStructureA.GetClusterInfos()[i];

			if (cluster.cycleLength > 2) {
				int tbCount = 0;
				for (int j = 0; j < cluster.cycleStates.size(); ++j) {
					int p = cluster.cycleStates[j];

					if (!clusterStructureB.IsBigCluster(clusterStructureB.GetVertexInfos()[p].clusterIndex))
						++tbCount;
				}

				if (tbCount > int(0.5 * cluster.cycleLength + 0.5))
					return false;
			}
			if (cluster.cycleLength == 2) {
				int dummyLetter = letters[0];
				auto statesPair = AutomataStatesPair(cluster.cycleStates[0], cluster.cycleStates[1], dummyLetter);

				std::set<int> states;
				auto pBqB = statesPair.GetNext(graph, letters[1]);
				states.insert(pBqB.GetP());
				states.insert(pBqB.GetQ());
				auto pB2qB2 = pBqB.GetNext(graph, letters[1]);
				states.insert(pB2qB2.GetP());
				states.insert(pB2qB2.GetQ());

				if (states.size() == 2)
					return false;

				if (states.size() == 3) {
					if (!clusterStructureA.IsBigCluster(clusterStructureA.GetVertexInfos()[pBqB.GetP()].clusterIndex) ||
						!clusterStructureA.IsBigCluster(clusterStructureA.GetVertexInfos()[pBqB.GetQ()].clusterIndex))
						return false;
				}

				if (states.size() == 4) {
					if ((!clusterStructureA.IsBigCluster(clusterStructureA.GetVertexInfos()[pBqB.GetP()].clusterIndex) ||
						!clusterStructureA.IsBigCluster(clusterStructureA.GetVertexInfos()[pBqB.GetQ()].clusterIndex)) &&
						(!clusterStructureA.IsBigCluster(clusterStructureA.GetVertexInfos()[pB2qB2.GetP()].clusterIndex) ||
						!clusterStructureA.IsBigCluster(clusterStructureA.GetVertexInfos()[pB2qB2.GetQ()].clusterIndex)))
						return false;
				}
			}
		}

		return true;
	}

	static std::vector<int> MoveSetByLetter(const Graph& graph, const std::vector<int>& states, int letter) {
		std::vector<int> ans;
		for (int i = 0; i < states.size(); ++i) {
			ans.push_back(graph[states[i]][letter]);
		}

		return ans;
	}

	static bool CheckIfSetIsInSmallClusters(const std::vector<int>& states, const ClusterStructure& clusterStructure) {
		for (int a : states) {
			if (clusterStructure.IsBigCluster(clusterStructure.GetVertexInfos()[a].clusterIndex))
				return false;
		}

		return true;
	}

	static bool CheckTheorem2Case2(const Graph& graph, const ClusterStructure& clusterStructureA, const ClusterStructure& clusterStructureB, const std::vector<int>& letters) {
		int letterB = letters[1];

		std::vector<bool> isCycleInTb(clusterStructureA.GetClusterCount());
		std::vector<bool> isImageOfCycleInTa(clusterStructureA.GetClusterCount());
		for (int clusterIndex = 0; clusterIndex < clusterStructureA.GetClusterCount(); ++clusterIndex) {
			auto cluster = clusterStructureA.GetClusterInfos()[clusterIndex];
			isCycleInTb[clusterIndex] = CheckIfSetIsInSmallClusters(cluster.cycleStates, clusterStructureB);

			isImageOfCycleInTa[clusterIndex] = CheckIfSetIsInSmallClusters(MoveSetByLetter(graph, cluster.cycleStates, letterB), clusterStructureA);
		}

		for (int clusterIndex1 = 0; clusterIndex1 < clusterStructureA.GetClusterCount(); ++clusterIndex1) {
			for (int clusterIndex2 = clusterIndex1 + 1; clusterIndex2 < clusterStructureA.GetClusterCount(); ++clusterIndex2) {
				auto clusterP = clusterStructureA.GetClusterInfos()[clusterIndex1];
				auto clusterQ = clusterStructureA.GetClusterInfos()[clusterIndex2];

				if (clusterStructureA.IsBigCluster(clusterIndex1) && clusterStructureA.IsBigCluster(clusterIndex2))
					continue;

				if (clusterP.cycleLength > clusterQ.cycleLength)
					std::swap(clusterP, clusterQ);

				if (clusterP.cycleLength == 1) {
					if ((isCycleInTb[clusterIndex1] || isCycleInTb[clusterIndex2]) && (isImageOfCycleInTa[clusterIndex1] || isImageOfCycleInTa[clusterIndex2]))
						return false;
				}
				else {
					int d = Utils::GreatestCommonDivisor(clusterP.cycleLength, clusterQ.cycleLength);
					
					std::vector<bool> Ip(d);
					for (int i = 0; i < d; ++i) {
						bool indexIsGood = true;
						for (int k = 0; k < clusterP.cycleLength / d; ++k) {
							int state = clusterP.cycleStates[(i + k * d) % clusterP.cycleLength];

							if (clusterStructureB.IsBigCluster(clusterStructureB.GetVertexInfos()[state].clusterIndex)) {
								indexIsGood = false;
								break;
							}
						}

						if (indexIsGood)
							Ip[i] = true;
					}

					std::vector<bool> Iq(d);
					int I_qSetHash = 0;
					for (int i = 0; i < d; ++i) {
						bool indexIsGood = true;
						for (int k = 0; k < clusterQ.cycleLength / d; ++k) {
							int state = clusterQ.cycleStates[(i + k * d) % clusterQ.cycleLength];

							if (clusterStructureB.IsBigCluster(clusterStructureB.GetVertexInfos()[state].clusterIndex)) {
								indexIsGood = false;
								break;
							}
						}

						if (indexIsGood)
							Iq[i] = true;
					}

					for (int x = 0; x < d; ++x) {
						std::vector<bool> Iqx(d);
						for (int i = 0; i < d; ++i) {
							Iqx[(i + x) % d] = Iq[i];
						}

						bool unionIsEntireZd = true;
						for (int i = 0; i < d; ++i) {
							unionIsEntireZd &= Ip[i] || Iqx[i];
						}

						if (unionIsEntireZd)
							return false;
					}
				}
			}
		}

		return true;
	}

public:
	static FastSynchrinizationTestResult IsSynchronizableFast(const Graph& graph, const std::vector<int>& letters) {
		if (letters.size() != 2)
			throw std::runtime_error("Expected two-letter automata.");

		int n = graph.size();
		auto condensation = CondensationBuilder::GetInstance().BuildCondensation(graph, letters);

		if (!condensation.IsSingleMin())
			return FastSynchrinizationTestResult::NonSynchronizable;

		double minimalComponentSize = double(n) / (4.0 * exponent * exponent);

		if (Utils::DoubleLess(condensation.GetMinComponentSize(), minimalComponentSize))
			return FastSynchrinizationTestResult::DontKnow;

		std::vector<ClusterStructure> clusterStructures;
		ClusterStructure clusterStructureWithSingleHighestTree;
		bool appropriateClusterStructureFound = false;
		int clusterCountThreshold = 5.0 * log(n);
		for (int i = 0; i < letters.size(); ++i) {
			int letter = letters[i];
			auto clusterStructure = ClusterStructure(graph, letter);
			clusterStructures.push_back(clusterStructure);

			if (clusterStructure.GetClusterCount() > clusterCountThreshold)
				return FastSynchrinizationTestResult::DontKnow;

			if (clusterStructure.IsSingleHighestTree() &&
				clusterStructure.DoesHighestOneCrownIntersectsWithCondensationMinComponent(condensation) &&
				clusterStructure.GetHighestOneCrownSize() > 2 * clusterStructure.GetHighestOneCrownRootsCount()) {

				clusterStructureWithSingleHighestTree = clusterStructure;
				appropriateClusterStructureFound = true;
			}
		}

		if (!appropriateClusterStructureFound)
			return FastSynchrinizationTestResult::DontKnow;

		auto stablePair = clusterStructureWithSingleHighestTree.GetStablePair();
		auto stablePairsSets = StablePairsSets(graph, stablePair, letters);

		if (!stablePairsSets.AreSetsCorrect())
			return FastSynchrinizationTestResult::DontKnow;

		for (int i = 0; i < letters.size(); ++i) {
			int letter = letters[i];
			auto clusterStructure = clusterStructures[i];
			auto stablePairs = stablePairsSets.GetStablePairs(letter);
			auto clusterGraph = ClusterGraph(clusterStructure, stablePairs);

			if (clusterGraph.IsColoringExists())
				return FastSynchrinizationTestResult::DontKnow;
		}

		if (!CheckTheorem2Case1(graph, clusterStructures[0], clusterStructures[1], letters) || !CheckTheorem2Case2(graph, clusterStructures[0], clusterStructures[1], letters))
			return FastSynchrinizationTestResult::DontKnow;

		return FastSynchrinizationTestResult::Synchronizable;
	}

public:
	static FastSynchronizationChecker& GetInstance() {
		static FastSynchronizationChecker instance;
		return instance;
	}

	bool IsSynchronizableFast(const Graph& graph, int sigma) const {
		for (int i = 0; i < sigma; ++i) {
			for (int j = i + 1; j < sigma; ++j) {
				std::vector<int> letters{i, j};

				auto twoLettersResult = IsSynchronizableFast(graph, letters);

				if (twoLettersResult != FastSynchrinizationTestResult::DontKnow)
					return twoLettersResult;
			}
		}

		return SlowSynchronizationChecker::GetInstance().IsSynchronizableSlow(graph, sigma);
	}
};
