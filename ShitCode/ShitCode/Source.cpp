#pragma once
#pragma comment(linker, "/STACK:16777216")

#include <iostream>
#include <ctime>
#include "TypesDeclarations.h"
#include "FastSynchronizationChecker.h"

using namespace std;

int main() {
	//freopen("input.txt", "rt", stdin);
	//freopen("output.txt", "wt", stdout);

	//Graph g;
	//int sigma;
	//Utils::ReadAutomaton(g, sigma, "input.txt");

	//vector<int> letters;
	//for (int i = 0; i < sigma; ++i) {
	//	letters.push_back(i);
	//}
	//auto condensation = CondensationBuilder::GetInstance().BuildCondensation(g, letters);
	//string path = "E:\\SynchronizingCheck\\ShitCode";
	//auto graph = Utils::GenerateRandomAutomaton(100, 2);
	//Utils::PrintInGraphViz(g, path + "\\output");
	////Utils::PrintInGraphViz(condensation.condensation, "E:\\SynchronizingCheck\\ShitCode\\condensation");

	//printf("%d", condensation.IsSingleMin());

	double start = clock();

	int syn = 0;

	int n = 1000;
	int sigma = 5;
	int sum = 0;
	for (int i = 0; i < 1000; ++i) {
		++sum;
		printf("%d\r", sum);


		auto graph = Utils::GenerateRandomAutomaton(n, sigma);

		auto result = FastSynchronizationChecker::GetInstance().IsSynchronizableFast(graph, sigma);

		//if (result == FastSynchrinizationTestResult::Synchronizable && !IsSynchronizableSlow(graph, sigma)){
		//	throw;
		//}

		if (result == false)
			++syn;
	}
	freopen("input.txt", "rt", stdin);
	freopen("output.txt", "wt", stdout);

	printf("%.3lf\n", (clock() - start) / CLOCKS_PER_SEC);

	printf("%d\n", sum);
	printf("%d\n", syn);

	return 0;
}
