﻿#pragma once
#include <vector>
#include "TypesDeclarations.h"

class SlowSynchronizationChecker {
	SlowSynchronizationChecker() {}

	SlowSynchronizationChecker(const SlowSynchronizationChecker&);
	SlowSynchronizationChecker& operator =(SlowSynchronizationChecker&);

	static Graph BuildInvertedSquaredAutomaton(const Graph& graph, int sigma) {
		int n = graph.size();
		Graph invertedSquaredAutomaton;
		invertedSquaredAutomaton.resize(n * n);

		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < n; ++j) {
				int v = i * n + j;

				for (int c = 0; c < sigma; ++c) {
					int toI = graph[i][c];
					int toJ = graph[j][c];

					int to = toI * n + toJ;
					invertedSquaredAutomaton[to].push_back(v);
				}
			}
		}

		return invertedSquaredAutomaton;
	}

public:
	static SlowSynchronizationChecker& GetInstance() {
		static SlowSynchronizationChecker instance;
		return instance;
	}

	bool IsSynchronizableSlow(const Graph& graph, int sigma) const {
		std::vector<bool> used;
		int n = graph.size();
		auto invertedSquaredAutomaton = BuildInvertedSquaredAutomaton(graph, sigma);

		used.assign(n * n, false);
		std::vector<int> q;
		for (int i = 0; i < n; ++i) {
			q.push_back(i * n + i);
			used[i * n + i] = true;
		}
		int l = 0;

		while (l < q.size()) {
			int v = q[l++];

			for (int i = 0; i < invertedSquaredAutomaton[v].size(); ++i) {
				int to = invertedSquaredAutomaton[v][i];

				if (used[to])
					continue;

				q.push_back(to);
				used[to] = true;
			}
		}

		for (int i = 0; i < used.size(); ++i) {
			if (!used[i])
				return false;
		}

		return true;
	}
};
