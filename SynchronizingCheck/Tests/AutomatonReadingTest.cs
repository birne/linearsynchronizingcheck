﻿using System;
using System.IO;

using Graph.GraphImplementations;
using Graph.InternalProperties;
using Graph.IO;

using NUnit.Framework;

// ReSharper disable PossibleNullReferenceException

namespace Tests
{
    [TestFixture]
    public class AutomatonReadingTest
    {
        [Test]
        public void Test()
        {
            var automaton = new Automaton();
            automaton.Read(new FileReader(GetFilePath("smallAutomaton.txt")));

            var v = automaton.GetVertex("2");
            var vNeighbours = automaton.GetNeighbours(v);

            Assert.That(vNeighbours[0].Id, Is.EqualTo("3"));
            Assert.That(vNeighbours[1].Id, Is.EqualTo("0"));

            var vEdges = automaton.GetAdjacentEdges(v);
            Assert.That(automaton.GetProperty<LetterProperty>(vEdges[0]).Letter, Is.EqualTo('a'));
            Assert.That(automaton.GetProperty<LetterProperty>(vEdges[1]).Letter, Is.EqualTo('b'));
        }

        private static string GetFilePath(string fileName)
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..\\..\\", "Files", fileName);
        }
    }
}