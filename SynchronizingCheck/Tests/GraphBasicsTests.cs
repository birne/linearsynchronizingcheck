﻿using System.Linq;

using Graph.GraphBasics;
using Graph.GraphImplementations;

using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class GraphBasicsTests
    {
        [Test]
        public void VertexInsertionTest()
        {
            var graph = new SimpleDynamicGraph();
            graph.AddVertex(new Vertex(0));

            Assert.That(graph.Verticies.Length, Is.EqualTo(1));
            Assert.That(graph.Verticies[0].Id, Is.EqualTo("0"));
        }

        [Test]
        public void EdgeInsertionTest()
        {
            var graph = new SimpleDynamicGraph();
            var from = new Vertex(1);
            var to = new Vertex(2);
            graph.AddEdge(new Edge("edge1", from, to));

            Assert.That(graph.Verticies.Length, Is.EqualTo(2));
            Assert.That(graph.Verticies[0].Id, Is.EqualTo("1"));
            Assert.That(graph.Verticies[1].Id, Is.EqualTo("2"));

            Assert.That(graph.Edges.Length, Is.EqualTo(1));
            Assert.That(graph.Edges[0].Id, Is.EqualTo("edge1"));
        }

        [Test]
        public void AdjacentEdgesExtractionTest()
        {
            var graph = InitSimpleGraph();
            const string pivotVertexId = "0";
            var edgeCount = graph.EdgesCount;

            var adjacentEdges = graph.GetAdjacentEdges(graph.GetVertex(pivotVertexId)).OrderBy(e => e.Id).ToArray();

            Assert.That(adjacentEdges.Length, Is.EqualTo(edgeCount));
            Assert.That(adjacentEdges[edgeCount - 1].To.Id, Is.EqualTo((edgeCount - 1).ToString()));
        }

        [Test]
        public void NeighboursExtractionTest()
        {
            var graph = InitSimpleGraph();
            const string pivotVertexId = "0";
            var verticiesCount = graph.VerticiesCount;

            var neighbours = graph.GetNeighbours(graph.GetVertex(pivotVertexId)).OrderBy(v => v.Id).ToArray();

            Assert.That(neighbours.Length, Is.EqualTo(verticiesCount));
            Assert.That(neighbours[2].Id, Is.EqualTo("2"));
        }

        private static IGraph InitSimpleGraph()
        {
            var graph = new SimpleDynamicGraph();
            const int vertexCount = 4;
            const int edgeCount = 4;
            const string pivotVertexId = "0";

            for(var vertexId = 0; vertexId < vertexCount; ++vertexId)
                graph.AddVertex(new Vertex(vertexId));

            for(var edgeId = 0; edgeId < edgeCount; ++edgeId)
            {
                var toVertexId = edgeId.ToString();
                graph.AddEdge(new Edge(edgeId, graph.GetVertex(pivotVertexId), graph.GetVertex(toVertexId)));
            }

            return graph;
        }
    }
}