﻿using System;
using System.IO;

using Graph.GraphImplementations;
using Graph.IO;

using GraphAlgorithms.AlgorithmImplementations;

using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class CondensationBuildingAlgorithmTest
    {
        [Test]
        public void Test()
        {
            var automaton = new Automaton();
            automaton.Read(new FileReader(GetFilePath("smallAutomaton.txt")));

            CondensationBuildingAlgorithm.Instance.Run(automaton);
        }

        private static string GetFilePath(string fileName)
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..\\..\\", "Files", fileName);
        }
    }
}