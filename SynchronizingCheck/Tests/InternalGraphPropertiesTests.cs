﻿using Graph.GraphBasics;
using Graph.InternalProperties;

using NUnit.Framework;

using Utils;

namespace Tests
{
    [TestFixture]
    public class InternalGraphPropertiesTests
    {
        [Test]
        public void InternalVertexPropertiesTest()
        {
            var graph = InitSimpleGraph();

            graph.SetProperty(graph.GetVertex("0"), new VertexPropety
                {
                    B = "abacaba"
                });

            Assert.That(graph.GetProperty<VertexPropety>(graph.GetVertex("0")).With(p => p.B), Is.EqualTo("abacaba"));
        }

        [Test]
        public void InternalEdgePropertiesTest()
        {
            var graph = InitSimpleGraph();

            graph.SetProperty(graph.GetEdge("0"), new EdgeProperty
                {
                    A = "abacaba"
                });

            Assert.That(graph.GetProperty<EdgeProperty>(graph.GetEdge("0")).With(p => p.A), Is.EqualTo("abacaba"));
        }

        private static IDynamicGraph InitSimpleGraph()
        {
            var graph = new TestGraph();
            const int vertexCount = 4;
            const int edgeCount = 4;
            const string pivotVertexId = "0";

            for(var vertexId = 0; vertexId < vertexCount; ++vertexId)
                graph.AddVertex(new Vertex(vertexId));

            for(var edgeId = 0; edgeId < edgeCount; ++edgeId)
            {
                var toVertexId = edgeId.ToString();
                graph.AddEdge(new Edge(edgeId, graph.GetVertex(pivotVertexId), graph.GetVertex(toVertexId)));
            }

            return graph;
        }

        private class EdgeProperty : InternalEdgeProperty
        {
            public string A { get; set; }
        }

        private class VertexPropety : InternalVertexProperty
        {
            public string B { get; set; }
        }

        private class TestGraph : DynamicGraph
        {
            public TestGraph()
                : base(new[] {typeof(VertexPropety)}, new[] {typeof(EdgeProperty)})
            {
            }
        }
    }
}