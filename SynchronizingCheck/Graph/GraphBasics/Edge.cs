﻿using System;

using JetBrains.Annotations;

namespace Graph.GraphBasics
{
    public class Edge
    {
        public Edge([NotNull] Vertex from, [NotNull] Vertex to)
        {
            Id = Guid.NewGuid().ToString();
            From = from;
            To = to;
        }

        public Edge([NotNull] string id, [NotNull] Vertex from, [NotNull] Vertex to)
        {
            Id = id;
            From = from;
            To = to;
        }

        public Edge(int id, [NotNull] Vertex from, [NotNull] Vertex to)
        {
            Id = id.ToString();
            From = from;
            To = to;
        }

        [NotNull]
        public string Id { get; private set; }

        [NotNull]
        public Vertex From { get; private set; }

        [NotNull]
        public Vertex To { get; private set; }
    }
}