﻿using System;

using Graph.InternalProperties;

using JetBrains.Annotations;

namespace Graph.GraphBasics
{
    public interface IGraph
    {
        [NotNull]
        Vertex[] Verticies { get; }

        [NotNull]
        Edge[] Edges { get; }

        int VerticiesCount { get; }

        int EdgesCount { get; }

        [NotNull]
        Type[] RegisteredVertexProperties { get; }

        [NotNull]
        Type[] RegisteredEdgeProperties { get; }

        [NotNull]
        Vertex GetVertex([NotNull] string id);

        [NotNull]
        Edge GetEdge([NotNull] string id);

        [NotNull]
        Edge[] GetAdjacentEdges([NotNull] Vertex vertex);

        [NotNull]
        Vertex[] GetNeighbours([NotNull] Vertex vertex);

        [CanBeNull]
        TVertexProperty GetProperty<TVertexProperty>([NotNull] Vertex vertex) where TVertexProperty : InternalVertexProperty;

        [CanBeNull]
        TEdgeProperty GetProperty<TEdgeProperty>([NotNull] Edge edge) where TEdgeProperty : InternalEdgeProperty;

        bool VertexExists([NotNull] Vertex vertex);

        bool EdgeExists([NotNull] Edge edge);
    }
}