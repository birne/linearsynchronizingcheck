﻿using Graph.InternalProperties;

using JetBrains.Annotations;

namespace Graph.GraphBasics
{
    public interface IDynamicGraph : IGraph
    {
        void AddEdge([NotNull] Edge edge);

        void AddVertex([NotNull] Vertex vertex);

        void SetProperty<TVertexProperty>([NotNull] Vertex vertex, [NotNull] TVertexProperty vertexProperty) where TVertexProperty : InternalVertexProperty;

        void SetProperty<TEdgeProperty>([NotNull] Edge edge, [NotNull] TEdgeProperty edgeProperty) where TEdgeProperty : InternalEdgeProperty;
    }
}