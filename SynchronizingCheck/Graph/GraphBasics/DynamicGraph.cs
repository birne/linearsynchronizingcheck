﻿using System;
using System.Collections.Generic;
using System.Linq;

using Graph.Exceptions;
using Graph.InternalProperties;

using JetBrains.Annotations;

namespace Graph.GraphBasics
{
    public class DynamicGraph : IDynamicGraph
    {
        public DynamicGraph()
        {
            internalVertexPropertiesRegistry = new HashSet<Type>();
            internalEdgePropertiesRegistry = new HashSet<Type>();
        }
        
        protected DynamicGraph([NotNull] Type[] internalVertexPropertyTypes, [NotNull] Type[] internalEdgePropertyTypes)
        {
            var badVertexType = internalVertexPropertyTypes.FirstOrDefault(t => !typeof(InternalVertexProperty).IsAssignableFrom(t));
            if(badVertexType != null)
                throw new BadGraphStateExeption(string.Format("Vertex property must inherit InternalVertexProperty class, but {0} is not.", badVertexType));
            var badEdgeType = internalEdgePropertyTypes.FirstOrDefault(t => !typeof(InternalEdgeProperty).IsAssignableFrom(t));
            if(badVertexType != null)
                throw new BadGraphStateExeption(string.Format("Edge property must inherit InternalEdgeProperty class, but {0} is not.", badEdgeType));

            internalVertexPropertiesRegistry = new HashSet<Type>(internalVertexPropertyTypes);
            internalEdgePropertiesRegistry = new HashSet<Type>(internalEdgePropertyTypes);

            foreach(var vertexPropertyType in internalVertexPropertyTypes)
                internalVertexProperties.Add(vertexPropertyType, new Dictionary<string, InternalVertexProperty>());
            foreach(var vertexEdgeType in internalEdgePropertyTypes)
                internalEdgeProperties.Add(vertexEdgeType, new Dictionary<string, InternalEdgeProperty>());
        }

        [NotNull]
        public Vertex[] Verticies { get { return verticies.Values.OrderBy(v => v.Id).ToArray(); } } //TODO: Think about speed up

        [NotNull]
        public Edge[] Edges { get { return edges.Values.OrderBy(v => v.Id).ToArray(); } } //TODO: Think about speed up

        public int VerticiesCount { get { return verticies.Count; } }
        public int EdgesCount { get { return edges.Count; } }

        [NotNull]
        public Type[] RegisteredVertexProperties { get { return internalVertexPropertiesRegistry.ToArray(); } }
        
        [NotNull]
        public Type[] RegisteredEdgeProperties { get { return internalEdgePropertiesRegistry.ToArray(); } }

        [NotNull]
        public Vertex GetVertex([NotNull] string id)
        {
            if(!verticies.ContainsKey(id))
                throw new BadGraphRequestExeption(string.Format("Vertex with Id : {0} is absent", id));

            return verticies[id];
        }

        [NotNull]
        public Edge GetEdge([NotNull] string id)
        {
            if(!edges.ContainsKey(id))
                throw new BadGraphRequestExeption(string.Format("Edge with Id : {0} is absent", id));

            return edges[id];
        }

        [NotNull]
        public Edge[] GetAdjacentEdges([NotNull] Vertex vertex)
        {
            if(!VertexExists(vertex))
                throw new BadGraphRequestExeption(string.Format("Vertex with Id : {0} is absent", vertex.Id));

            return adjacencyLists[vertex.Id].ToArray();
        }

        [NotNull]
        public Vertex[] GetNeighbours([NotNull] Vertex vertex)
        {
            if(!VertexExists(vertex))
                throw new BadGraphRequestExeption(string.Format("Vertex with Id : {0} is absent", vertex.Id));

            return adjacencyLists[vertex.Id].Select(e => e.To).ToArray();
        }

        [CanBeNull]
        public TVertexProperty GetProperty<TVertexProperty>([NotNull] Vertex vertex) where TVertexProperty : InternalVertexProperty
        {
            var propertyType = typeof(TVertexProperty);

            if(!internalVertexPropertiesRegistry.Contains(propertyType))
                throw new BadGraphRequestExeption(string.Format("Vertex property {0} us not registered in graph", propertyType));

            if(!internalVertexProperties[propertyType].ContainsKey(vertex.Id))
                return null;

            return internalVertexProperties[propertyType][vertex.Id] as TVertexProperty;
        }

        [CanBeNull]
        public TEdgeProperty GetProperty<TEdgeProperty>([NotNull] Edge edge) where TEdgeProperty : InternalEdgeProperty
        {
            var propertyType = typeof(TEdgeProperty);

            if(!internalEdgePropertiesRegistry.Contains(propertyType))
                throw new BadGraphRequestExeption(string.Format("Edge property {0} us not registered in graph", propertyType));

            if(!internalEdgeProperties[propertyType].ContainsKey(edge.Id))
                return null;

            return internalEdgeProperties[propertyType][edge.Id] as TEdgeProperty;
        }

        public void AddEdge([NotNull] Edge edge)
        {
            if(EdgeExists(edge))
                throw new BadGraphStateExeption(string.Format("Edge with Id : {0} already exists", edge.Id));

            if(!VertexExists(edge.From))
                AddVertex(edge.From);
            if(!VertexExists(edge.To))
                AddVertex(edge.To);

            edges.Add(edge.Id, edge);
            adjacencyLists[edge.From.Id].Add(edge);
        }

        public void AddVertex([NotNull] Vertex vertex)
        {
            if(VertexExists(vertex))
                throw new BadGraphStateExeption(string.Format("Vertex with Id : {0} already exists", vertex.Id));

            verticies.Add(vertex.Id, vertex);
            adjacencyLists.Add(vertex.Id, new List<Edge>());
        }

        public void SetProperty<TVertexProperty>([NotNull] Vertex vertex, [NotNull] TVertexProperty vertexProperty) where TVertexProperty : InternalVertexProperty
        {
            if(!VertexExists(vertex))
                throw new BadGraphStateExeption(string.Format("Can't set property. Vertex with Id : {0} is absent", vertex.Id));
            var propertyType = vertexProperty.GetType();
            if(!internalVertexPropertiesRegistry.Contains(propertyType))
                throw new BadGraphRequestExeption(string.Format("Vertex property {0} us not registered in graph", propertyType));

            if(!internalVertexProperties[propertyType].ContainsKey(vertex.Id))
                internalVertexProperties[propertyType].Add(vertex.Id, vertexProperty);

            internalVertexProperties[propertyType][vertex.Id] = vertexProperty;
        }

        public void SetProperty<TEdgeProperty>([NotNull] Edge edge, [NotNull] TEdgeProperty edgeProperty) where TEdgeProperty : InternalEdgeProperty
        {
            if(!EdgeExists(edge))
                throw new BadGraphStateExeption(string.Format("Can't set property. Edge with Id : {0} is absent", edge.Id));
            var propertyType = edgeProperty.GetType();
            if(!internalEdgePropertiesRegistry.Contains(propertyType))
                throw new BadGraphRequestExeption(string.Format("Edge property {0} us not registered in graph", propertyType));

            if(!internalEdgeProperties[propertyType].ContainsKey(edge.Id))
                internalEdgeProperties[propertyType].Add(edge.Id, edgeProperty);

            internalEdgeProperties[propertyType][edge.Id] = edgeProperty;
        }

        public bool VertexExists([NotNull] Vertex vertex)
        {
            return verticies.ContainsKey(vertex.Id);
        }

        public bool EdgeExists([NotNull] Edge edge)
        {
            return edges.ContainsKey(edge.Id);
        }

        private readonly Dictionary<string, Vertex> verticies = new Dictionary<string, Vertex>();
        private readonly Dictionary<string, Edge> edges = new Dictionary<string, Edge>();
        private readonly Dictionary<string, List<Edge>> adjacencyLists = new Dictionary<string, List<Edge>>();
        private readonly Dictionary<Type, Dictionary<string, InternalVertexProperty>> internalVertexProperties = new Dictionary<Type, Dictionary<string, InternalVertexProperty>>();
        private readonly Dictionary<Type, Dictionary<string, InternalEdgeProperty>> internalEdgeProperties = new Dictionary<Type, Dictionary<string, InternalEdgeProperty>>();
        private readonly HashSet<Type> internalVertexPropertiesRegistry;
        private readonly HashSet<Type> internalEdgePropertiesRegistry;
    }
}