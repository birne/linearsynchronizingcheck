﻿using System;

using JetBrains.Annotations;

namespace Graph.GraphBasics
{
    public class Vertex
    {
        public Vertex()
        {
            Id = Guid.NewGuid().ToString();
        }

        public Vertex([NotNull] string id)
        {
            Id = id;
        }

        public Vertex(int id)
        {
            Id = id.ToString();
        }

        [NotNull]
        public string Id { get; private set; }
    }
}