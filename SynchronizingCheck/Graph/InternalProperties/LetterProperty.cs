﻿namespace Graph.InternalProperties
{
    public class LetterProperty : InternalEdgeProperty
    {
        public char Letter { get; set; }
    }
}