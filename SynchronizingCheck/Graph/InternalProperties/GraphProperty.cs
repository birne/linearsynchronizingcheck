﻿using Graph.GraphBasics;

namespace Graph.InternalProperties
{
    public class GraphProperty : InternalVertexProperty
    {
        public IGraph Graph { get; set; }
    }
}