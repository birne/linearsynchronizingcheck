﻿namespace Graph.IO
{
    public interface IFileReader
    {
        int NextInt();
        char NextChar();
    }
}