using System;
using System.IO;

using JetBrains.Annotations;

namespace Graph.IO
{
    public class FileReader : IFileReader
    {
        public FileReader([NotNull] string path)
        {
            fileContent = File.ReadAllText(path);
            currentPosition = 0;
        }

        public int NextInt()
        {
            SkipWhitespaces();
            if(!HasNext())
                throw new Exception("End if file reached");

            var result = 0;
            while(HasNext() && fileContent[currentPosition] >= '0' && fileContent[currentPosition] <= '9')
                result = result * 10 + fileContent[currentPosition++] - '0';

            return result;
        }

        public char NextChar()
        {
            SkipWhitespaces();
            if(!HasNext())
                throw new Exception("End if file reached");

            return fileContent[currentPosition++];
        }

        private void SkipWhitespaces()
        {
            while(HasNext() && fileContent[currentPosition] <= ' ')
                ++currentPosition;
        }

        private bool HasNext()
        {
            return currentPosition < fileContent.Length;
        }

        private readonly string fileContent;
        private int currentPosition;
    }
}