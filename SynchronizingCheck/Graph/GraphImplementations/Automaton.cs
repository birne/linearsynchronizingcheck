﻿using System;

using Graph.GraphBasics;
using Graph.InternalProperties;
using Graph.IO;

using JetBrains.Annotations;

namespace Graph.GraphImplementations
{
    public class Automaton : DynamicGraph
    {
        public Automaton()
            : base(new Type[0], new[] {typeof(LetterProperty)})
        {
        }
    }

    public static class AutomatonExtensions
    {
        public static void Read([NotNull] this IDynamicGraph graph, [NotNull] IFileReader fileReader)
        {
            // ReSharper disable once UnusedVariable
            var n = fileReader.NextInt();
            var m = fileReader.NextInt();

            for(var i = 0; i < m; ++i)
            {
                var from = fileReader.NextInt();
                var to = fileReader.NextInt();
                var letter = fileReader.NextChar();

                var newEdge = new Edge(i, new Vertex(from), new Vertex(to));
                graph.AddEdge(newEdge);
                graph.SetProperty(newEdge, new LetterProperty {Letter = letter});
            }
        }
    }
}