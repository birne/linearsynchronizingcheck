﻿using System;

using Graph.GraphBasics;
using Graph.InternalProperties;

namespace Graph.GraphImplementations
{
    public class Condensation : DynamicGraph
    {
        public Condensation() 
        :base(new []{typeof(GraphProperty)}, new Type[0])
        { }
    }
}