﻿using System;

using JetBrains.Annotations;

namespace Graph.Exceptions
{
    public class BadGraphRequestExeption : Exception
    {
        public BadGraphRequestExeption([NotNull] string errorMessage)
            : base(errorMessage)
        {
        }
    }
}