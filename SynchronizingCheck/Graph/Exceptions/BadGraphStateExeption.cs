﻿using System;

using JetBrains.Annotations;

namespace Graph.Exceptions
{
    public class BadGraphStateExeption : Exception
    {
        public BadGraphStateExeption([NotNull] string errorMessage)
            : base(errorMessage)
        {
        }
    }
}