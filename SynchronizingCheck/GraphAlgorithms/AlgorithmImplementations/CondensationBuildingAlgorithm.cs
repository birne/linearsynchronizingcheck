﻿using System.Collections.Generic;

using Graph.GraphBasics;
using Graph.GraphImplementations;

using GraphAlgorithms.AlgorithmBasics;
using GraphAlgorithms.ExternalGraphProperties;

using JetBrains.Annotations;

using Utils;

namespace GraphAlgorithms.AlgorithmImplementations
{
    public sealed class CondensationBuildingAlgorithm : IGraphAlgorithm<Condensation>
    {
        private CondensationBuildingAlgorithm()
        {
        }

        public static CondensationBuildingAlgorithm Instance { get { return instance; } }

        public Condensation Run([NotNull] IGraph graph)
        {
            var properties = new CondensationBuildingProperties(graph);
            var order = new List<Vertex>();

            foreach(var vertex in graph.Verticies)
            {
                if(properties.GetProperty<ColorProperty>(vertex).Return(p => p.Color, 0) == 0)
                    OrderingDfs(graph, properties, vertex, order);
            }
            order.Reverse();

            properties.InitializeVertexProperty(new ColorProperty(0));
            var currentColor = 1;
            foreach(var vertex in order)
            {
                if(properties.GetProperty<ColorProperty>(vertex).Return(p => p.Color, 0) == 0)
                    ColorComponent(GraphInversionAlgorithm.Instance.Run(graph), properties, vertex, currentColor++);
            }

            var result = new Condensation();
            //TODO: Incomplete
            return result;
        }

        private static void ColorComponent(IGraph graph, CondensationBuildingProperties properties, Vertex vertex, int currentColor)
        {
            properties.SetProperty(vertex, new ColorProperty(currentColor));

            foreach(var toVertex in graph.GetNeighbours(vertex))
            {
                if(properties.GetProperty<ColorProperty>(toVertex).Return(p => p.Color, 0) == 0)
                    ColorComponent(graph, properties, toVertex, currentColor);
            }
        }

        private static void OrderingDfs(IGraph graph, CondensationBuildingProperties properties, Vertex vertex, List<Vertex> order)
        {
            properties.SetProperty(vertex, new ColorProperty(1));

            foreach(var toVertex in graph.GetNeighbours(vertex))
            {
                if(properties.GetProperty<ColorProperty>(toVertex).Return(p => p.Color, 0) == 0)
                    OrderingDfs(graph, properties, toVertex, order);
            }

            order.Add(vertex);
        }

        private static readonly CondensationBuildingAlgorithm instance = new CondensationBuildingAlgorithm();
    }
}