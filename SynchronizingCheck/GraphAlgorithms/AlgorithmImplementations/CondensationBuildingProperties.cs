﻿using System;

using Graph.GraphBasics;

using GraphAlgorithms.AlgorithmBasics;
using GraphAlgorithms.ExternalGraphProperties;

using JetBrains.Annotations;

namespace GraphAlgorithms.AlgorithmImplementations
{
    public class CondensationBuildingProperties : GraphProperties
    {
        public CondensationBuildingProperties([NotNull] IGraph graph)
            : base(new[] {typeof(ColorProperty)}, new Type[0], graph)
        {
        }
    }
}