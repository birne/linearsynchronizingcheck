﻿using Graph.GraphBasics;

using GraphAlgorithms.AlgorithmBasics;

using JetBrains.Annotations;

namespace GraphAlgorithms.AlgorithmImplementations
{
    public sealed class GraphInversionAlgorithm : IGraphAlgorithm<IGraph>
    {
        private GraphInversionAlgorithm()
        {
        }

        public static GraphInversionAlgorithm Instance { get { return instance; } }

        [NotNull]
        public IGraph Run([NotNull] IGraph graph)
        {
            var result = new DynamicGraph();
            foreach(var vertex in graph.Verticies)
            {
                result.AddVertex(vertex);
            }
            foreach(var edge in graph.Edges)
            {
                result.AddEdge(new Edge(edge.Id, edge.To, edge.From));
            }

            return result;
        }

        private static readonly GraphInversionAlgorithm instance = new GraphInversionAlgorithm();
    }
}