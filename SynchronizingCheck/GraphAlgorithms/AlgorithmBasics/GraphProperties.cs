using System;
using System.Collections.Generic;
using System.Linq;

using Graph.Exceptions;
using Graph.GraphBasics;

using GraphAlgorithms.ExternalGraphProperties;

using JetBrains.Annotations;

namespace GraphAlgorithms.AlgorithmBasics
{
    public class GraphProperties : IGraphProperties
    {
        protected GraphProperties([NotNull] IGraph graph)
        {
            vertexPropertiesRegistry = new HashSet<Type>();
            edgePropertiesRegistry = new HashSet<Type>();
            this.graph = graph;
        }

        protected GraphProperties([NotNull] Type[] vertexPropertyTypes, [NotNull] Type[] edgePropertyTypes, [NotNull] IGraph graph)
        {
            this.graph = graph;

            var badVertexType = vertexPropertyTypes.FirstOrDefault(t => !typeof(VertexProperty).IsAssignableFrom(t));
            if(badVertexType != null)
                throw new BadGraphStateExeption(string.Format("Vertex property must inherit VertexProperty class, but {0} is not.", badVertexType));
            var badEdgeType = edgePropertyTypes.FirstOrDefault(t => !typeof(EdgeProperty).IsAssignableFrom(t));
            if(badVertexType != null)
                throw new BadGraphStateExeption(string.Format("Edge property must inherit EdgeProperty class, but {0} is not.", badEdgeType));

            vertexPropertiesRegistry = new HashSet<Type>(vertexPropertyTypes);
            edgePropertiesRegistry = new HashSet<Type>(edgePropertyTypes);

            foreach(var vertexPropertyType in vertexPropertyTypes)
                vertexProperties.Add(vertexPropertyType, new Dictionary<string, VertexProperty>());
            foreach(var vertexEdgeType in edgePropertyTypes)
                edgeProperties.Add(vertexEdgeType, new Dictionary<string, EdgeProperty>());
        }

        [CanBeNull]
        public TVertexProperty GetProperty<TVertexProperty>([NotNull] Vertex vertex) where TVertexProperty : VertexProperty
        {
            var propertyType = typeof(TVertexProperty);

            if(!vertexPropertiesRegistry.Contains(propertyType))
                throw new BadGraphRequestExeption(string.Format("Vertex property {0} us not registered in graph", propertyType));

            if(!vertexProperties[propertyType].ContainsKey(vertex.Id))
                return null;

            return vertexProperties[propertyType][vertex.Id] as TVertexProperty;
        }

        [CanBeNull]
        public TEdgeProperty GetProperty<TEdgeProperty>([NotNull] Edge edge) where TEdgeProperty : EdgeProperty
        {
            var propertyType = typeof(TEdgeProperty);

            if(!edgePropertiesRegistry.Contains(propertyType))
                throw new BadGraphRequestExeption(string.Format("Edge property {0} us not registered in graph", propertyType));

            if(!edgeProperties[propertyType].ContainsKey(edge.Id))
                return null;

            return edgeProperties[propertyType][edge.Id] as TEdgeProperty;
        }

        public void SetProperty<TVertexProperty>([NotNull] Vertex vertex, [NotNull] TVertexProperty vertexProperty) where TVertexProperty : VertexProperty
        {
            if(!graph.VertexExists(vertex))
                throw new BadGraphStateExeption(string.Format("Can't set property. Vertex with Id : {0} is absent", vertex.Id));
            var propertyType = vertexProperty.GetType();
            if(!vertexPropertiesRegistry.Contains(propertyType))
                throw new BadGraphRequestExeption(string.Format("Vertex property {0} us not registered in graph", propertyType));

            if(!vertexProperties[propertyType].ContainsKey(vertex.Id))
                vertexProperties[propertyType].Add(vertex.Id, vertexProperty);

            vertexProperties[propertyType][vertex.Id] = vertexProperty;
        }

        public void SetProperty<TEdgeProperty>([NotNull] Edge edge, [NotNull] TEdgeProperty edgeProperty) where TEdgeProperty : EdgeProperty
        {
            if(!graph.EdgeExists(edge))
                throw new BadGraphStateExeption(string.Format("Can't set property. Edge with Id : {0} is absent", edge.Id));
            var propertyType = edgeProperty.GetType();
            if(!edgePropertiesRegistry.Contains(propertyType))
                throw new BadGraphRequestExeption(string.Format("Edge property {0} us not registered in graph", propertyType));

            if(!edgeProperties[propertyType].ContainsKey(edge.Id))
                edgeProperties[propertyType].Add(edge.Id, edgeProperty);

            edgeProperties[propertyType][edge.Id] = edgeProperty;
        }

        public void InitializeVertexProperty<TVertexProperty>(TVertexProperty vertexProperty) where TVertexProperty : VertexProperty
        {
            var propertyType = vertexProperty.GetType();
            if(!vertexPropertiesRegistry.Contains(propertyType))
                throw new BadGraphRequestExeption(string.Format("Vertex property {0} us not registered in graph", propertyType));

            foreach(var vertex in graph.Verticies)
                vertexProperties[propertyType][vertex.Id] = vertexProperty;
        }

        public void InitializeEdgeProperty<TEdgeProperty>(TEdgeProperty edgeProperty) where TEdgeProperty : EdgeProperty
        {
            var propertyType = edgeProperty.GetType();
            if(!edgePropertiesRegistry.Contains(propertyType))
                throw new BadGraphRequestExeption(string.Format("Edge property {0} us not registered in graph", propertyType));

            foreach(var edge in graph.Edges)
                edgeProperties[propertyType][edge.Id] = edgeProperty;
        }

        private readonly Dictionary<Type, Dictionary<string, VertexProperty>> vertexProperties = new Dictionary<Type, Dictionary<string, VertexProperty>>();
        private readonly Dictionary<Type, Dictionary<string, EdgeProperty>> edgeProperties = new Dictionary<Type, Dictionary<string, EdgeProperty>>();
        private readonly HashSet<Type> vertexPropertiesRegistry;
        private readonly HashSet<Type> edgePropertiesRegistry;
        private readonly IGraph graph;
    }
}