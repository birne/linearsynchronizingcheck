﻿using Graph.GraphBasics;

namespace GraphAlgorithms.AlgorithmBasics
{
    public interface IGraphAlgorithm<out TOutput>
    {
        TOutput Run(IGraph graph);
    }
}