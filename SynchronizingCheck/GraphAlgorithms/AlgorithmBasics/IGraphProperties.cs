﻿using Graph.GraphBasics;

using GraphAlgorithms.ExternalGraphProperties;

using JetBrains.Annotations;

namespace GraphAlgorithms.AlgorithmBasics
{
    public interface IGraphProperties
    {
        [CanBeNull]
        TVertexProperty GetProperty<TVertexProperty>([NotNull] Vertex vertex) where TVertexProperty : VertexProperty;

        [CanBeNull]
        TEdgeProperty GetProperty<TEdgeProperty>([NotNull] Edge edge) where TEdgeProperty : EdgeProperty;

        void SetProperty<TVertexProperty>([NotNull] Vertex vertex, [NotNull] TVertexProperty vertexProperty) where TVertexProperty : VertexProperty;

        void SetProperty<TEdgeProperty>([NotNull] Edge edge, [NotNull] TEdgeProperty edgeProperty) where TEdgeProperty : EdgeProperty;

        void InitializeVertexProperty<TVertexProperty>([NotNull] TVertexProperty vertexProperty) where TVertexProperty : VertexProperty;
        
        void InitializeEdgeProperty<TEdgeProperty>([NotNull] TEdgeProperty edgeProperty) where TEdgeProperty : EdgeProperty;
    }
}