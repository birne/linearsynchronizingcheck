﻿namespace GraphAlgorithms.ExternalGraphProperties
{
    public class ColorProperty : VertexProperty
    {
        public ColorProperty(int color)
        {
            Color = color;
        }

        public int Color { get; private set; }
    }
}